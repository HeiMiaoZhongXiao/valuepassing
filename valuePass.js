// pages/demo/valuePass/valuePass.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    adress:[
      {
        id:100,
        name: "我是内容1"
      }, {
        id: 101,
        name: "我是内容2"
      }, {
        id: 102,
        name: "我是内容3"
      }
    ],
    testData: { name: 'username', password: 'password' },
    list: ['item-A', 'item-B']  
  },
  delete:function(e){
    var index = parseInt(e.currentTarget.dataset.index); 
    console.log("index:" + index); 
    //取出值
    var that = this;
    var address = that.data.adress[index];
    // 给出确认提示框 
    wx.showModal({   
      title: '确认',   
      content: '要删除这个地址吗？',   
      success: function(res) {     
        console.log("删除成功");
      } 
    }) 
  },
  navTo:function(e){
    var that = this;
    var index = parseInt(e.currentTarget.dataset.index); 
    var id = that.data.adress[index].id;
    wx.navigateTo({
      url: 'valueGet?id='+id,
    })
  },
  navToObject:function(e){
    wx.navigateTo({
      url: 'valueGet?testData=' + JSON.stringify(this.data.testData)
    })
  },
  navToObject1: function (e) {
    wx.navigateTo({
      url: 'valueGet?list=' + JSON.stringify(this.data.list)
    })
  },
  formSubmit:function(e){
    // detail   
    var detail = e.detail.value.detail;   
    console.log("detail:"+detail);
    // realname   
    var realname = e.detail.value.realname; 
    console.log("realname:" + realname);  
    // mobile   
    var mobile = e.detail.value.mobile; 
    console.log("mobile:" + mobile);
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})